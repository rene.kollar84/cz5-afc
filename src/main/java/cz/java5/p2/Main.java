package cz.java5.p2;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String... args) {
        List<String> sourceList = Arrays.asList("A", "B", "C", "D","a");
        List<String> sortedList = orderList(sourceList);
        System.out.println(sortedList);


    }

    public static List<String> orderList(List<String> sourceList) {
        //equivalent to code below
//        Collections.sort(sourceList, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o2.compareToIgnoreCase(o1);
//            }
//        });
        Collections.sort(sourceList, (l1,l2)-> l2.compareToIgnoreCase(l1));
        return sourceList;
    }

}
