package cz.java5.p5;

public interface HashSet<E> {

    HashSet<E> add(E item);

    void remove(E item);

    int size();

    boolean contains(E item);

    void clear();

}
