package cz.java5.p5;
/**
 * Implement the
 * SDAHashSet<E> class that will implement the HashSet<E> logic. It should support methods:
 * •
 * add
 * •
 * remove
 * •
 * size
 * •
 * contains
 * •
 * clear
 */
public class Main {
    public static void main(String[] args) {
        HashSet<String> stringSDAHashSet = new SDAHashSet<>();
        HashSet<String> newHashSet = stringSDAHashSet.add("zelenina")
                        .add("ovoce")
                        .add("maso")
                         .add("mleko");

        System.out.println(stringSDAHashSet.equals(newHashSet));

        System.out.println(stringSDAHashSet.size());
        System.out.println(stringSDAHashSet.contains("maso"));
        System.out.println(stringSDAHashSet.contains("ryby"));
        stringSDAHashSet.remove("zelenina");
        System.out.println(stringSDAHashSet.contains("zelenina"));
        stringSDAHashSet.clear();
        System.out.println(stringSDAHashSet.size());
    }
}
