package cz.java5.p4;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        storage.addToStorage("zelenina", "meloun");
        storage.addToStorage("zelenina", "mrkev");
        storage.addToStorage("ovoce", "jablko");
        storage.printAll();
        System.out.println();

        System.out.println("findValues");
        storage.findValues("meloun");
        System.out.println("findValues");
        storage.findValues2("meloun");
    }
}
