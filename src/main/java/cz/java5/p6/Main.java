package cz.java5.p6;

import java.util.Map;
import java.util.TreeMap;

/**
 * Create a method that accepts
 * TreeMap and prints the first and last EntrySet in the console.
 */
public class Main {
    public static void main(String[] args) {
        TreeMap<String, String> map = new TreeMap<>();
        map.put("jidlo", "chleba");
        map.put("nabytek", "zidle");
        map.put("auto", "mercedez");
        map.put("zvire", "pes");
        printTree(map);

    }
    public static void printTree(TreeMap<String, String> treeMap) {
        Map.Entry<String, String> firstEntry = treeMap.firstEntry();
        System.out.println(firstEntry);
        Map.Entry<String, String> lastEntry = treeMap.lastEntry();
        System.out.println(lastEntry);
    }
}
